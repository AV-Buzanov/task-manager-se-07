package ru.buzanov.tm.command;

import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.enumerated.RoleType;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;
    protected BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public abstract boolean isSecure() throws Exception;

    public boolean isRoleAllow(RoleType role) {
        return true;
    }

}
