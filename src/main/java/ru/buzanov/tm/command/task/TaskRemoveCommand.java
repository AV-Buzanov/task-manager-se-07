package ru.buzanov.tm.command.task;

import ru.buzanov.tm.command.AbstractCommand;

public class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE TASK TO REMOVE]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getTaskService().getList(userId));
        String idBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        serviceLocator.getTaskService().remove(userId, idBuf);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
