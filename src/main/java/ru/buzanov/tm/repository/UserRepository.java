package ru.buzanov.tm.repository;

import ru.buzanov.tm.api.IUserRepository;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {
    private Map<String, User> map = new LinkedHashMap<>();

    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (User user : findAll()) {
            s.append(indexBuf).append(". ").append(user.getLogin());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getIdByCount(final int count) {
        int indexBuf = 1;
        for (User user : findAll()) {
            if (indexBuf == count)
                return user.getId();
            indexBuf++;
        }
        return null;
    }

    public User findByLogin(final String login) {
        if (isLoginExist(login)) {
            for (User user : findAll()) {
                if (user.getLogin().equals(login))
                    return user;
            }
        }
        return null;
    }

    public Collection<User> findByRole(final RoleType role) {
        Collection<User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getRoleType().equals(role))
                users.add(user);
        }
        return users;
    }

    public boolean isPassCorrect(final String login, final String pass) {
        return pass.equals(findByLogin(login).getPasswordHash());
    }

    public boolean isLoginExist(final String login) {
        for (User user : findAll()) {
            if (user.getLogin().equals(login))
                return true;
        }
        return false;
    }

    public void merge(final String id, final User user) {
        if (user.getLogin() != null)
            this.map.get(id).setLogin(user.getLogin());
        if (user.getPasswordHash() != null)
            this.map.get(id).setPasswordHash(user.getPasswordHash());
        if (user.getRoleType() != null)
            this.map.get(id).setRoleType(user.getRoleType());
        if (user.getName() != null)
            this.map.get(id).setName(user.getName());
    }
}
