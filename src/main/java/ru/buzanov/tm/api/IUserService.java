package ru.buzanov.tm.api;

import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService extends IService<User> {

    Collection<User> findAll();

    User findOne(String id);

    void merge(String id, User user);

    User remove(String id);

    void removeAll();

    User findByLogin(String login);

    boolean isPassCorrect(String login, String pass);

    boolean isLoginExist(String login);

    User getCurrentUser();

    void setCurrentUser(User user);

    Collection<User> findByRole(RoleType role);

    User load(User user);
}
