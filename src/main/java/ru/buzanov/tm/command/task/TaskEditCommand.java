package ru.buzanov.tm.command.task;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;

public class TaskEditCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-edit";
    }

    @Override
    public String description() {
        return "Edit task";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println("[CHOOSE TASK TO EDIT]");

        System.out.println(serviceLocator.getTaskService().getList(userId));
        stringBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Task task = serviceLocator.getTaskService().findOne(userId, stringBuf);
        System.out.println("[NAME]");
        System.out.println(task.getName());
        System.out.println("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (serviceLocator.getTaskService().isNameExist(userId, stringBuf)) {
            System.out.println("Task with this name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
            task.setName(stringBuf);
        System.out.println("[START DATE]");
        if (task.getStartDate() != null)
            System.out.println(DateUtil.dateFormat().format(task.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[END DATE]");
        if (task.getEndDate() != null)
            System.out.println(DateUtil.dateFormat().format(task.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[DESCRIPTION]");
        if (task.getDescription() != null)
            System.out.println(task.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setDescription(stringBuf);
        System.out.println("[PROJECT]");
        if (task.getProjectId() != null)

            System.out.println(serviceLocator.getProjectService().findOne(userId, task.getProjectId()).getName());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        System.out.println(serviceLocator.getProjectService().getList(userId));
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        serviceLocator.getTaskService().merge(userId, task.getId(), task);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
