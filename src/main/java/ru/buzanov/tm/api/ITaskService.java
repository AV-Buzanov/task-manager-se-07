package ru.buzanov.tm.api;

import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends ISubjectService<Task> {

    Collection<Task> findAll();

    Task findOne(String id);

    boolean isNameExist(String userId, String name);

    String getList();

    String getIdByCount(int count);

    Task load(Task task);

    void merge(String id, Task task);

    Task remove(String id);

    void removeAll();

    void removeAll(String userId);

    Collection<Task> findByProjectId(String projectId);

    void removeByProjectId(String projectId);

    Collection<Task> findAll(String userId);

    Task findOne(String userId, String id);

    String getList(String userId);

    String getIdByCount(String userId, int count);

    void merge(String userId, String id, Task task);

    Task remove(String userId, String id);

    Collection<Task> findByProjectId(String userId, String projectId);

    void removeByProjectId(String userId, String projectId);
}
