package ru.buzanov.tm.service;

import ru.buzanov.tm.api.ISubjectRepository;
import ru.buzanov.tm.entity.AbstractSubjectEntity;

import java.util.Collection;

public abstract class AbstractSubjectService<T extends AbstractSubjectEntity> extends AbstractService<T> {
    protected ISubjectRepository<T> abstractSubjectRepository;

    public AbstractSubjectService() {
    }

    public AbstractSubjectService(final ISubjectRepository<T> abstractRepository) {
        super(abstractRepository);
        this.abstractSubjectRepository = abstractRepository;
    }

    public Collection<T> findAll(final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.findAll(userId);
    }

    public T findOne(final String userId, final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.findOne(userId, id);
    }

    public boolean isNameExist(final String userId, final String name) {
        if (userId == null || userId.isEmpty())
            return false;
        if (name == null || name.isEmpty())
            return false;
        return abstractSubjectRepository.isNameExist(userId, name);
    }

    public String getList() {
        return abstractSubjectRepository.getList();
    }

    public String getList(final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getList(userId);
    }

    public String getIdByCount(final int count) {
        return abstractSubjectRepository.getIdByCount(count);
    }

    public String getIdByCount(final String userId, int count) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getIdByCount(userId, count);
    }

    @Override
    public T load(final T entity) {
        return abstractSubjectRepository.load(entity);
    }

    @Override
    public void merge(final String id, final T project) {
        abstractSubjectRepository.merge(id, project);
    }

    public void merge(final String userId, final String id, final T entity) {
        if (userId == null || userId.isEmpty())
            return;
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractSubjectRepository.merge(userId, id, entity);
    }

    public T remove(final String userId, final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.remove(userId, id);
    }

    public void removeAll(final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        abstractSubjectRepository.removeAll(userId);
    }
}
