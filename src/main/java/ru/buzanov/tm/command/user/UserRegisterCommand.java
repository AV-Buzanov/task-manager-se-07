package ru.buzanov.tm.command.user;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.entity.User;

public class UserRegisterCommand extends AbstractCommand {
    @Override
    public String command() {
        return "register";
    }

    @Override
    public String description() {
        return "User registration";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        System.out.println("[REGISTRATION]");
        System.out.println("[ENTER LOGIN]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            System.out.println("Login can't be empty");
            return;
        }
        if (serviceLocator.getUserService().isLoginExist(stringBuf)) {
            System.out.println("This login already exist");
            return;
        }
        User user = new User();
        user.setLogin(stringBuf);
        System.out.println("[ENTER PASS]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty() || stringBuf.length() < 6) {
            System.out.println("Pass can't be empty and less then 6 symbols");
            return;
        }
        System.out.println("[REPEAT PASS]");

        if (!stringBuf.equals(reader.readLine())) {
            System.out.println("Invalid pass");
            return;
        }
        user.setPasswordHash(stringBuf);
        System.out.println("[ENTER YOUR NAME]");
        stringBuf = reader.readLine();
        user.setName(stringBuf);
        user.setRoleType(RoleType.USER);
        serviceLocator.getUserService().load(user);
        System.out.println("[Hello, " + user.getName() + ", now type auth to log in]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
