package ru.buzanov.tm.command.project;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;

public class ProjectClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {

        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        for (Project project : serviceLocator.getProjectService().findAll(userId))
            serviceLocator.getTaskService().removeByProjectId(userId, project.getId());
        serviceLocator.getProjectService().removeAll(userId);

        System.out.println("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
