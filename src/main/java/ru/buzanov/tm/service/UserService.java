package ru.buzanov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.buzanov.tm.api.IUserRepository;
import ru.buzanov.tm.api.IUserService;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public class UserService extends AbstractService<User> implements IUserService {
    private IUserRepository userRepository;
    private User currentUser;

    public UserService() {
    }

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User load(final User user) {
        user.setPasswordHash(DigestUtils.md5Hex(user.getPasswordHash()));
        return userRepository.load(user);
    }

    @Override
    public void merge(final String id, final User user) {
        if (id == null || user == null || id.isEmpty())
            return;
        user.setPasswordHash(DigestUtils.md5Hex(user.getPasswordHash()));
        userRepository.merge(id, user);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty())
            return null;
        return userRepository.findByLogin(login);
    }

    public boolean isPassCorrect(final String login, final String pass) {
        if (login == null || login.isEmpty())
            return false;
        if (pass == null || pass.isEmpty())
            return false;
        return userRepository.isPassCorrect(login, DigestUtils.md5Hex(pass));
    }

    public boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty())
            return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    public Collection<User> findByRole(final RoleType role) {
        if (role == null)
            return null;
        return userRepository.findByRole(role);
    }
}