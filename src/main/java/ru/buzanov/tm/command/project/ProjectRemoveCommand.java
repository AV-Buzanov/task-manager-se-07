package ru.buzanov.tm.command.project;

import ru.buzanov.tm.command.AbstractCommand;

public class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO REMOVE]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getProjectService().getList(userId));
        String idBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        serviceLocator.getProjectService().remove(userId, idBuf);
        serviceLocator.getTaskService().removeByProjectId(userId, idBuf);

        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
