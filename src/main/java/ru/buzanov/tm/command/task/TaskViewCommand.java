package ru.buzanov.tm.command.task;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;

public class TaskViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE TASK TO VIEW]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getTaskService().getList(userId));
        String idBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Task task = serviceLocator.getTaskService().findOne(userId, idBuf);
        System.out.println("[NAME]");
        System.out.println(task.getName());
        System.out.println("[START DATE]");
        if (task.getStartDate() != null)
            System.out.println(DateUtil.dateFormat().format(task.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[END DATE]");
        if (task.getEndDate() != null)
            System.out.println(DateUtil.dateFormat().format(task.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[DESCRIPTION]");
        if (task.getDescription() != null)
            System.out.println(task.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[PROJECT]");
        if (task.getProjectId() != null)
            System.out.println(serviceLocator.getProjectService().findOne(userId, task.getProjectId()).getName());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
