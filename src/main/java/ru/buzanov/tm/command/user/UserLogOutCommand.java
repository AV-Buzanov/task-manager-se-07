package ru.buzanov.tm.command.user;

import ru.buzanov.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "log-out";
    }

    @Override
    public String description() {
        return "User log-out";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().setCurrentUser(null);
        System.out.println("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
