package ru.buzanov.tm.command.task;

import ru.buzanov.tm.command.AbstractCommand;

public class TaskListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getTaskService().getList(userId));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
