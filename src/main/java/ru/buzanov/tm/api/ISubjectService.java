package ru.buzanov.tm.api;

import java.util.Collection;

public interface ISubjectService<T> extends IService<T> {

    Collection<T> findAll(String userId);

    T findOne(String userId, String id);

    boolean isNameExist(String userId, String name);

    String getList();

    String getList(String userId);

    String getIdByCount(int count);

    String getIdByCount(String userId, int count);

    T load(T project);

    void merge(String userId, String id, T project);

    T remove(String userId, String id);

    void removeAll(String userId);
}
