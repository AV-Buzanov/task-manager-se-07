package ru.buzanov.tm.entity;

import java.util.Date;

public class Project extends AbstractSubjectEntity {
    private String description;
    private Date startDate;
    private Date endDate;

    public Project() {
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}