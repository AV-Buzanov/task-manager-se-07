package ru.buzanov.tm.repository;

import ru.buzanov.tm.api.IProjectRepository;
import ru.buzanov.tm.entity.Project;

public class ProjectRepository extends AbstractSubjectRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
    }

    public void merge(final String id, final Project project) {
        if (project.getName() != null)
            map.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            map.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            map.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            map.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            map.get(id).setUserId(project.getUserId());
    }

    public void merge(final String userId, final String id, final Project project) {
        if (!userId.equals(map.get(id).getUserId()))
            return;
        if (project.getName() != null)
            map.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            map.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            map.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            map.get(id).setDescription(project.getDescription());
        if (project.getUserId() != null)
            map.get(id).setUserId(project.getUserId());
    }
}