package ru.buzanov.tm.command.user;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

public class UserListCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-list";
    }

    @Override
    public String description() {
        return "View user list (for admin only)";
    }

    @Override
    public void execute() throws Exception {

        for (User user:serviceLocator.getUserService().findAll()){
            System.out.println("login:"+user.getLogin()+" name:"+user.getName()+" role:"+user.getRoleType().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.USER.equals(role))
        return false;
        return super.isRoleAllow(role);
    }
}
