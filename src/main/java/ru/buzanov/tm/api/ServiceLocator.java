package ru.buzanov.tm.api;

import ru.buzanov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ServiceLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    Collection<AbstractCommand> getCommands();
}
