package ru.buzanov.tm.bootstrap;

import org.reflections.Reflections;
import ru.buzanov.tm.api.*;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.*;
import ru.buzanov.tm.service.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public final class Bootstrap implements ServiceLocator {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IUserService userService = new UserService(userRepository);
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.buzanov.tm").getSubTypesOf(AbstractCommand.class);

    private void init() throws IllegalAccessException, InstantiationException {
        for (Class clas : classes) {
            registryCommand((AbstractCommand) clas.newInstance());
        }
        registryUser(new User("UserName", "user", "123456", RoleType.USER));
        registryUser(new User("AdminName", "admin", "123456", RoleType.ADMIN));
    }

    private void registryCommand(final AbstractCommand command) {
        if (command.command() == null || command.command().isEmpty()) {
            return;
        }
        if (command.description() == null || command.description().isEmpty()) {
            return;
        }

        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void registryUser(final User user) {
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            return;
        }
        if (user.getPasswordHash() == null || user.getPasswordHash().isEmpty()) {
            return;
        }
        if (user.getName() == null || user.getName().isEmpty()) {
            return;
        }
        if (user.getRoleType() == null) {
            return;
        }
        userService.load(user);
    }

    public void start() {
        try {
            init();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        String command = "";

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (!commands.get("exit").command().equals(command)) {
            try {
                command = reader.readLine();
                if (!command.isEmpty()) {

                    if (userService.getCurrentUser() == null && commands.get(command).isSecure())
                        System.out.println("Authorise, please");
                    else {
                        if (userService.getCurrentUser() != null && !commands.get(command).isRoleAllow(userService.getCurrentUser().getRoleType()))
                            System.out.println("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }
                }

            } catch (ParseException e) {
                System.out.println("Wrong input, try again");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selected index doesn't exist");
            } catch (Exception e) {
                System.out.println("Something wrong, try again");
            }
        }
    }

    public IUserRepository getUserRepository() {
        return userRepository;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}