package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> {

    protected final Map<String, T> map = new LinkedHashMap<>();

    public T load(final T entity) {
        return map.put(entity.getId(), entity);
    }

    public Collection<T> findAll() {
        return map.values();
    }

    public T findOne(final String id) {
        if (map.containsKey(id))
            return map.get(id);
        else return null;
    }

    public abstract void merge(final String id, final T entity);

    public T remove(final String id) {
        return map.remove(id);
    }

    public void removeAll() {
        map.clear();
    }
}
