package ru.buzanov.tm.command;

import com.jcabi.manifests.Manifests;

public class AboutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "Information about application";
    }

    @Override
    public void execute() throws Exception {

        System.out.println("Task-Manager ver.SE-07, " + Manifests.read("buildNumber"));
        System.out.println("Developer: " + Manifests.read("developer"));
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
