package ru.buzanov.tm.command.user;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;

public class UserEditCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-edit";
    }

    @Override
    public String description() {
        return "Edit user data";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT USER DATA]");
        System.out.println("[ENTER PASS TO EDIT]");
        String stringBuf;
        User user = serviceLocator.getUserService().getCurrentUser();
        stringBuf = reader.readLine();
        if (!serviceLocator.getUserService().isPassCorrect(user.getLogin(), stringBuf)) {
            System.out.println("Wrong pass");
            return;
        }
        System.out.println("[LOGIN]");
        System.out.println(user.getLogin());
        System.out.println("[ENTER NEW LOGIN]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            if (serviceLocator.getUserService().isLoginExist(stringBuf)) {
                System.out.println("User with this login already exist.");
                return;
            }
            user.setLogin(stringBuf);
        }
        System.out.println("[NAME]");
        System.out.println(user.getName());
        System.out.println("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            user.setName(stringBuf);
        }
        System.out.println("[ENTER NEW PASS IF NECESSARY]");
        stringBuf = reader.readLine();

        if (!stringBuf.isEmpty()) {
            if (stringBuf.length() > 6) {
                System.out.println("[REPEAT NEW PASS]");
                if (stringBuf.equals(reader.readLine())) {
                    user.setPasswordHash(stringBuf);
                } else
                    System.out.println("Pass not match");
            } else
                System.out.println("Pass can't be less then 6 symbols");
        }
        //serviceLocator.getUserService().merge(serviceLocator.getUserService().getCurrentUser().getId(), user);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
