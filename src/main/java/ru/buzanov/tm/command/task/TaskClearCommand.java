package ru.buzanov.tm.command.task;

import ru.buzanov.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().removeAll(userId);
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
