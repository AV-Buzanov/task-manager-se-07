package ru.buzanov.tm.command.project;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        String stringBuf;
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            System.out.println("Project name can't be empty");
            return;
        }

        if (serviceLocator.getProjectService().isNameExist(userId, stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        project.setName(stringBuf);
        System.out.println("[ENTER PROJECT START DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[ENTER PROJECT END DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);
        project.setUserId(userId);
        serviceLocator.getProjectService().load(project);

        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
