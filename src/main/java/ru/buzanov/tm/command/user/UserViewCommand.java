package ru.buzanov.tm.command.user;

import ru.buzanov.tm.command.AbstractCommand;

public class UserViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "View user information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[VIEW USER]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println("[LOGIN]");
        System.out.println(serviceLocator.getUserService().getCurrentUser().getLogin());
        System.out.println("[NAME]");
        System.out.println(serviceLocator.getUserService().getCurrentUser().getName());
        System.out.println("[ROLE]");
        System.out.println(serviceLocator.getUserService().getCurrentUser().getRoleType().displayName());
        System.out.println("[PROJECTS COUNT]");
        System.out.println(serviceLocator.getProjectService().findAll(userId).size());
        System.out.println("[TASKS COUNT]");
        System.out.println(serviceLocator.getTaskService().findAll(userId).size());
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
