package ru.buzanov.tm.entity;

import ru.buzanov.tm.enumerated.RoleType;

public class User extends AbstractEntity {
    private String name;
    private String login;
    private String passwordHash;
    private RoleType roleType;

    public User() {
    }

    public User(String name, String login, String pass, RoleType roleType) {
        this.name = name;
        this.login = login;
        this.passwordHash = pass;
        this.roleType = roleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
