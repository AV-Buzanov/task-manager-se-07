package ru.buzanov.tm.command.project;

import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;

public class ProjectEditCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHOOSE PROJECT TO EDIT]");
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println(serviceLocator.getProjectService().getList(userId));
        String stringBuf;
        stringBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Project project = serviceLocator.getProjectService().findOne(userId, stringBuf);
        System.out.println("[NAME]");
        System.out.println(project.getName());
        System.out.println("[ENTER NEW NAME]");
        stringBuf = reader.readLine();

        if (serviceLocator.getProjectService().isNameExist(userId, stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
            project.setName(stringBuf);

        System.out.println("[START DATE]");
        if (project.getStartDate() != null)
            System.out.println(DateUtil.dateFormat().format(project.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[END DATE]");
        if (project.getEndDate() != null)
            System.out.println(DateUtil.dateFormat().format(project.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        System.out.println("[DESCRIPTION]");
        if (project.getDescription() != null)
            System.out.println(project.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);

        serviceLocator.getProjectService().merge(userId, project.getId(), project);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
