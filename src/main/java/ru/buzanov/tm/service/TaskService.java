package ru.buzanov.tm.service;

import ru.buzanov.tm.api.ITaskRepository;
import ru.buzanov.tm.api.ITaskService;
import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public class TaskService extends AbstractSubjectService<Task> implements ITaskService {
    public TaskService() {
    }

    private ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    public Collection<Task> findByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty())
            return null;
        return this.taskRepository.findByProjectId(projectId);
    }

    public void removeByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(projectId);
    }

    public Collection<Task> findByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty())
            return null;
        if (projectId == null || projectId.isEmpty())
            return null;
        return taskRepository.findByProjectId(userId, projectId);
    }

    public void removeByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty())
            return;
        if (projectId == null || projectId.isEmpty())
            return;
        taskRepository.removeByProjectId(userId, projectId);
    }
}