package ru.buzanov.tm.service;

import ru.buzanov.tm.api.IRepository;
import ru.buzanov.tm.entity.AbstractEntity;

import java.util.Collection;

public abstract class AbstractService<T extends AbstractEntity> {

    protected IRepository<T> abstractRepository;

    public AbstractService() {
    }

    public AbstractService(final IRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    public T load(final T entity) {
        return abstractRepository.load(entity);
    }

    public Collection<T> findAll() {
        return abstractRepository.findAll();
    }

    public T findOne(final String id) {
        if (id == null || id.isEmpty())
            return null;
        return abstractRepository.findOne(id);
    }

    public void merge(final String id, final T entity) {
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractRepository.merge(id, entity);
    }

    public T remove(final String id) {
        if (id == null || id.isEmpty())
            return null;
        return abstractRepository.remove(id);
    }

    public void removeAll() {
        abstractRepository.removeAll();
    }
}
