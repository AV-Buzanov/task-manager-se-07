package ru.buzanov.tm.entity;

public abstract class AbstractSubjectEntity extends AbstractEntity {
    private String name;
    private String userId;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
