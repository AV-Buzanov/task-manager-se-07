package ru.buzanov.tm.api;

import ru.buzanov.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends ISubjectRepository<Task> {

    Collection<Task> findAll();

    Collection<Task> findAll(String userId);

    Task findOne(String id);

    Task findOne(String userId, String id);

    boolean isNameExist(String userId, String name);

    String getList();

    String getList(String userId);

    String getIdByCount(int count);

    String getIdByCount(String userId, int count);

    Task load(Task task);

    void merge(String id, Task task);

    void merge(String userId, String id, Task task);

    Task remove(String id);

    Task remove(String userId, String id);

    void removeAll();

    void removeAll(String userId);

    Collection<Task> findByProjectId(String projectId);

    void removeByProjectId(String projectId);

    Collection<Task> findByProjectId(String userId, String projectId);

    void removeByProjectId(String userId, String projectId);
}
