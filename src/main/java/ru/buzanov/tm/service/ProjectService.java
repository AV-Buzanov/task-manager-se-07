package ru.buzanov.tm.service;

import ru.buzanov.tm.api.IProjectRepository;
import ru.buzanov.tm.api.IProjectService;
import ru.buzanov.tm.entity.Project;

public class ProjectService extends AbstractSubjectService<Project> implements IProjectService {
    public ProjectService() {
    }

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
    }
}