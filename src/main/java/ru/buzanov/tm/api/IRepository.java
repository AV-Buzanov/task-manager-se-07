package ru.buzanov.tm.api;

import java.util.Collection;

public interface IRepository<T> {
    Collection<T> findAll();

    T findOne(String id);

    void merge(String id, T entity);

    T remove(String id);

    void removeAll();

    T load(T entity);
}
