package ru.buzanov.tm.repository;

import ru.buzanov.tm.api.ITaskRepository;
import ru.buzanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;


public class TaskRepository extends AbstractSubjectRepository<Task> implements ITaskRepository {

    public TaskRepository() {
    }

    public void merge(final String id, final Task task) {
        if (task.getName() != null)
            map.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            map.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            map.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            map.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            map.get(id).setUserId(task.getUserId());
    }

    public void merge(final String userId, final String id, final Task task) {
        if (!userId.equals(map.get(id).getUserId()))
            return;
        if (task.getName() != null)
            map.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            map.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            map.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            map.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            map.get(id).setUserId(task.getUserId());
    }

    public Collection<Task> findByProjectId(final String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(final String projectId) {
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }

    public Collection<Task> findByProjectId(final String userId, final String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(final String userId, final String projectId) {
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }
}
