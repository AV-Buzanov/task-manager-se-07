package ru.buzanov.tm.api;

import ru.buzanov.tm.entity.Project;

import java.util.Collection;

public interface IProjectRepository extends ISubjectRepository<Project> {

    Collection<Project> findAll();

    Collection<Project> findAll(String userId);

    Project findOne(String id);

    Project findOne(String userId, String id);

    boolean isNameExist(String userId, String name);

    String getList();

    String getList(String userId);

    String getIdByCount(int count);

    String getIdByCount(String userId, int count);

    Project load(Project project);

    void merge(String id, Project project);

    void merge(String userId, String id, Project project);

    Project remove(String id);

    Project remove(String userId, String id);

    void removeAll();

    void removeAll(String userId);
}

