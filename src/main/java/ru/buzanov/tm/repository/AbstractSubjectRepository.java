package ru.buzanov.tm.repository;

import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.AbstractSubjectEntity;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractSubjectRepository<T extends AbstractSubjectEntity> extends AbstractRepository<T> {

    public Collection<T> findAll(final String userId) {
        Collection<T> list = new ArrayList<>();
        for (T entity : map.values()) {
            if (userId.equals(entity.getUserId()))
                list.add(entity);
        }
        return list;
    }

    public T findOne(final String userId, final String id) {
        if (map.containsKey(id) && userId.equals(map.get(id).getUserId()))
            return map.get(id);
        else return null;
    }

    public boolean isNameExist(final String userId, final String name) {
        for (T entity : findAll(userId)) {
            if (name.equals(entity.getName()))
                return true;
        }
        return false;
    }

    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (T entity : findAll()) {
            s.append(indexBuf).append(". ").append(entity.getName());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getList(final String userId) {
        if (findAll(userId).isEmpty())
            return FormatConst.EMPTY_FIELD;
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (T entity : findAll(userId)) {
            s.append(indexBuf).append(". ").append(entity.getName());
            if (findAll(userId).size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    public String getIdByCount(final int count) {
        int indexBuf = 1;
        for (T entity : findAll()) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    public String getIdByCount(final String userId, final int count) {
        int indexBuf = 1;
        for (T entity : findAll(userId)) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    public abstract void merge(final String userId, final String id, final T entity);

    public T remove(final String userId, final String id) {
        if (userId.equals(map.get(id).getUserId()))
            return map.remove(id);
        return null;
    }

    public void removeAll(final String userId) {
        for (T entity : findAll(userId))
            remove(entity.getId());
    }
}
