package ru.buzanov.tm.enumerated;

public enum RoleType {
    ADMIN("Администратор"),
    USER("Пользователь");

    private String name;

    RoleType(String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }
}
